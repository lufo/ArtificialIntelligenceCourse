# -*- coding: utf-8 -*-
__author__ = 'lufo'

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import PCA
from sklearn.preprocessing import normalize
import numpy as np
import os
import time

NUMBER_OF_TRAIN_TEXT = 246
NUMBER_OF_TEST_TEXT = 1000
EMOTION_LIST = ['anger', 'disgust', 'fear', 'joy', 'sad', 'surprise']


def get_text_feature():
    """
    获取训练集和测试集文本的特征
    :return: narray,特征,i行j列表示第i个文本第j个特征的值
    """
    stop_words = []
    with open('stopwords.txt') as fr:
        for word in fr.readlines():
            stop_words.append(word.strip())
    stop_words = set(stop_words)
    vectorizer = CountVectorizer(min_df=1)

    with open('Dataset_words.txt') as fr:
        corpus = []
        for text in fr.readlines()[1:]:
            corpus.append(' '.join(set(text.strip().split('\t')[1].split()) - stop_words))

    train_feature_mat = vectorizer.fit_transform(corpus[:NUMBER_OF_TRAIN_TEXT])
    test_feature_mat = vectorizer.transform(corpus[NUMBER_OF_TRAIN_TEXT:])
    feature_mat = np.concatenate((train_feature_mat.toarray(), test_feature_mat.toarray()))
    feature_mat = np.array(map(lambda x: map(float, x), feature_mat))
    return normalize(feature_mat, norm='l1')


def get_train_emotion():
    """
    获取训练集各个文本的情感分布,结果进行l1归一化
    :return: array,i行j列表示第i个训练集在第j个情感上的概率,情感分别是['anger', 'disgust', 'fear', 'joy', 'sad', 'surprise']
    """
    emotion_mat = []
    for emotion in EMOTION_LIST:
        temp = []
        with open(''.join(['Dataset_words_', emotion, '.txt'])) as fr:
            for text in fr.readlines()[1:NUMBER_OF_TRAIN_TEXT + 1]:  # 有246个训练文本
                temp.append(float(text.strip().split('\t')[2]))
        emotion_mat.append(temp)
    return normalize(np.array(emotion_mat).transpose(), norm='l1')


def pca(n_components, data):
    """
    对数据进行pca降维
    :param n_components: 保留的特征数
    :param data: 原始数据
    :return: 降维后的数据
    """
    pca = PCA(n_components=n_components)
    new_data = pca.fit_transform(data)
    return new_data


def knn(k, train, train_label, test):
    """
    用knn预测
    :return: array,i行j列为第i个测试文本属于第j个情感的概率
    """
    predict_emotion_mat = np.zeros((NUMBER_OF_TEST_TEXT, 6))
    for i, test_item in enumerate(test):
        dis = []
        for train_item in train:
            dis.append(np.linalg.norm(test_item - train_item))  # 欧氏距离
        nearest_item = range(NUMBER_OF_TRAIN_TEXT)
        nearest_item.sort(key=lambda x: dis[x])
        for index in nearest_item[:k]:
            predict_emotion_mat[i] += train_label[index]
    return normalize(predict_emotion_mat, norm='l1')


def nb(train, train_label, test):
    """
    使用朴素贝叶斯预测
    :return: array,i行j列为第i个测试文本属于第j个情感的概率
    """
    train = train.transpose()
    word_emotion_mat = np.dot(train, train_label)  # i行j列为第i个单词与对应的情感的相关度
    word_count = np.sum(train)
    for i, word in enumerate(word_emotion_mat):
        word_emotion_mat[i] = word * np.sum(train[i]) / word_count
    predict_emotion_mat = np.dot(test, word_emotion_mat)
    return normalize(predict_emotion_mat, norm='l1')


def write_to_file(predict_emotion_mat, path):
    """
    将预测结果写入path中的文件,每个情感写入一个文件,如joy写入joy_predict.txt,每行为预测的概率
    :param predict_emotion_mat: array,i行j列为第i个测试文本属于第j个情感的概率
    """
    for i, emotion in enumerate(EMOTION_LIST):
        with open(os.path.join(path, emotion + '_predict.txt'), 'w') as fw:
            for p in predict_emotion_mat[:, i]:
                fw.write(str(p) + '\n')


def main():
    feature_mat = get_text_feature()
    # feature_mat = pca(100, feature_mat)
    emotion_mat = get_train_emotion()
    # predict_emotion_mat = knn(10, feature_mat[:NUMBER_OF_TRAIN_TEXT], emotion_mat, feature_mat[NUMBER_OF_TRAIN_TEXT:])
    predict_emotion_mat = nb(feature_mat[:NUMBER_OF_TRAIN_TEXT], emotion_mat, feature_mat[NUMBER_OF_TRAIN_TEXT:])
    write_to_file(predict_emotion_mat, '/Users/lufo/PycharmProjects/AI/Lab2/predict_test')


if __name__ == '__main__':
    start = time.clock()
    main()
    end = time.clock()
    print end - start
